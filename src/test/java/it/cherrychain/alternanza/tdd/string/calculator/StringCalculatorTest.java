package it.cherrychain.alternanza.tdd.string.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringCalculatorTest {

    private StringCalculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new StringCalculator();
    }

    @Test
    void return_0_if_input_string_is_empty() {
        assertEquals(0, calculator.add(""));
    }

    @Test
    void sum_of_a_number() {
        assertEquals(1, calculator.add("1"));
    }

    @Test
    void sum_of_two_numbers() {
        assertEquals(3, calculator.add("1,2"));
    }

    @Test
    void sum_of_many_numbers() {
        assertEquals(6, calculator.add("1,2,3"));
    }

    @Test
    void evaluate_new_line_as_delimiter() {
        assertEquals(6, calculator.add("1\n2,3"));
    }

}