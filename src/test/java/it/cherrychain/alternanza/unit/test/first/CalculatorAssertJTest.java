package it.cherrychain.alternanza.unit.test.first;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

//utilizzo delle asserzioni di assertj che migliorano la leggibilità
public class CalculatorAssertJTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    void get_sum_of_two_numbers() throws Calculator.NegativeNumberExcepiton {
        assertThat(calculator.sum(1, 2)).isEqualTo(3);
    }

    @Test
    void get_sum_of_many_numbers() throws Calculator.NegativeNumberExcepiton {
        assertThat(calculator.sum(asList(1, 1, 1))).isEqualTo(3);
    }

    @Test
    void throw_an_exception_when_sum_negative_numbers() {
        assertThatThrownBy(()->{ calculator.sum(asList(-1)); })
                .isInstanceOf(Calculator.NegativeNumberExcepiton.class);
    }
}