package it.cherrychain.alternanza.unit.test.second.injection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StoreTest {

    private Store store;

    @Mock
    private Products products;

    //viene chiamata all'inizio di ogni test
    @BeforeEach
    void setUp() {
        store = new Store(products);
    }

    //test su funzione sell
    //verifico che avvenga l'interazione con il collaboratore products
    @Test
    void should_sell_a_product() {
        //lancio la funzione che dovrò testare
        store.sell(1, 1.0);

        //verifico che products venga chiamato
        verify(products).add(any());
    }

    //test su funzione sell
    //verifico che avvenga l'interazione con il collaboratore products
    //specificando come sarà formato products
    @Test
    void should_sell_a_product_version_2() {
        //lancio la funzione che dovrò testare
        store.sell(1, 1.0);

        //verifico che products venga chiamato
        verify(products).add(new Product(1, 1.0, 1.0));
    }


    //test sul calcolo del prezzo medio
    @Test
    void should_get_average_of_price() {
        //programmo il comportamento della funzione getAll (sono io a decidere l'output)
        when(products.getAll()).thenReturn(
                asList(
                        new Product(2, 2.0, 1.0),
                        new Product(2, 1.0, 1.0)
                )
        );

        Double avg = store.averagePriceFor("name");

        //verifico che la mia funzione produca il risultato che mi aspetto
        assertEquals(1.5, avg);
    }
}