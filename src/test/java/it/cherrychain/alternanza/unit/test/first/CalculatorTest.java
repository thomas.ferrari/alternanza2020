package it.cherrychain.alternanza.unit.test.first;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

//utilizzo delle asserzioni di junit5
public class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    void get_sum_of_two_numbers() throws Calculator.NegativeNumberExcepiton {
        assertEquals(3, calculator.sum(1, 2));
    }

    @Test
    void get_sum_of_many_numbers() throws Calculator.NegativeNumberExcepiton {
        assertEquals(3, calculator.sum(asList(1, 1, 1)));
    }

    @Test
    void throw_an_exception_when_sum_negative_numbers() {
        assertThrows(Calculator.NegativeNumberExcepiton.class, ()->{ calculator.sum(asList(-1)); });
    }
}
