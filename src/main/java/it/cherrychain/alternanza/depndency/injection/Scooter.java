package it.cherrychain.alternanza.depndency.injection;

import it.cherrychain.alternanza.depndency.injection.engine.Engine50;
import it.cherrychain.alternanza.depndency.injection.silencer.OriginalSilencer;

public class Scooter {

    public int maxSpeed() {
        return new Engine50().declaredSpeed() + new OriginalSilencer().speedIncrement();
    }

    public static void main(String[] args) {
        Scooter myScooter = new Scooter();
        System.out.println("Velocità massima raggiunta: " + myScooter.maxSpeed());
    }

}
