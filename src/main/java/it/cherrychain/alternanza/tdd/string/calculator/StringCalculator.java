package it.cherrychain.alternanza.tdd.string.calculator;

import static java.util.Arrays.stream;

public class StringCalculator {

    public static final String DELIMITERS = ",|\n";

    public int add(String numbers) {
        if (existNumbers(numbers)) {
            return sumOf(numbers.split(DELIMITERS));
        }
        return 0;
    }

    private boolean existNumbers(String numbers) {
        return !numbers.isEmpty();
    }

    private int sumOf(String[] splittedNumbers) {
        return stream(splittedNumbers)
                .mapToInt(Integer::parseInt).sum();
    }
}
