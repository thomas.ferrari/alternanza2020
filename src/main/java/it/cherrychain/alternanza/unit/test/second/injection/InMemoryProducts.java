package it.cherrychain.alternanza.unit.test.second.injection;

import java.util.List;

import static java.util.Arrays.asList;

public class InMemoryProducts implements Products {

    private final List<Product> products = asList();

    public void add(Product product) {
        products.add(product);
    }

    public List<Product> getAll() {
        return products;
    }
}
