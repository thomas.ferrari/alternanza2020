package it.cherrychain.alternanza.unit.test.second.injection;

public class Store {

    private final Products products;

    public Store(Products products) {
        this.products = products;
    }

    public void sell(Integer quantity, Double price) {
        Double totalAmount  = quantity * price;
        products.add(new Product(quantity, price, totalAmount));
    }

    public Double averagePriceFor(String productName) {
        return products.getAll().stream()
                .map(Product::price)
                .mapToDouble(a -> a)
                .average()
                .orElse(0.0);
    }
}
