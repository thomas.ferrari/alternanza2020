package it.cherrychain.alternanza.unit.test.second.injection;

import java.util.List;

public interface Products {
    void add(Product product);

    List<Product> getAll();
}
