package it.cherrychain.alternanza.unit.test.second.injection;

import java.util.Objects;

public class Product {
    private final Integer quantity;
    private final Double price;
    private final Double totalAmount;

    public Product(Integer quantity, Double price, Double totalAmount) {
        this.quantity = quantity;
        this.price = price;
        this.totalAmount = totalAmount;
    }

    public double price() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(quantity, product.quantity) &&
                Objects.equals(price, product.price) &&
                Objects.equals(totalAmount, product.totalAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, price, totalAmount);
    }

    @Override
    public String toString() {
        return "Product{" +
                "quantity=" + quantity +
                ", price=" + price +
                ", totalAmount=" + totalAmount +
                '}';
    }
}
